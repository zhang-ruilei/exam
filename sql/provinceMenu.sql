-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市', '3', '1', 'province', 'system/province/index', 1, 0, 'C', '0', '0', 'system:province:list', '#', 'admin', sysdate(), '', null, '省市菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:province:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:province:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:province:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:province:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('省市导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:province:export',       '#', 'admin', sysdate(), '', null, '');