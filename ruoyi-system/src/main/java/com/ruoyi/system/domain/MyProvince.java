package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 省市对象 my_province
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
public class MyProvince extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 省市名称 */
    @Excel(name = "省市名称")
    private String city;

    /** $column.columnComment */
    private Long id;

    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("city", getCity())
            .append("id", getId())
            .toString();
    }
}
