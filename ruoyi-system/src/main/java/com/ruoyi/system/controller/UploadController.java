package com.ruoyi.system.controller;

import com.ruoyi.system.domain.MySchoolProfession;
import com.ruoyi.system.service.impl.MySchoolProfessionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/exam")
public class UploadController {
    @Autowired(required = false)
    private MySchoolProfessionServiceImpl schoolProfessionService;

    @PreAuthorize("@ss.hasPermi('exam:download')")
    @RequestMapping("/uploadFile")
    public String upload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String fileName = file.getOriginalFilename();
//        String filePath = "/path/to/save/" + fileName;
        String filePath = "D:\\path\\to\\save\\" + fileName;
        File dest = new File(filePath);
        try {
            file.transferTo(dest);
            return "上传成功";
        } catch (IOException e) {
            e.printStackTrace();
            return "上传失败，请重试";
        } finally {
            // 在这里可以添加其他操作，比如将文件信息保存到数据库等。
        }
    }

    @PreAuthorize("@ss.hasPermi('exam:download')")
    @PostMapping("/findSchool")
    public List<MySchoolProfession> findSchool(@RequestBody String place) {
        return schoolProfessionService.selectMySchoolProfessionByPlace(place);
    }

    @PreAuthorize("@ss.hasPermi('exam:download')")
    @PostMapping("/getfilelist")
    public String[] getfilelist(@RequestBody Map<String, String> placeSchoolName) {
        String place = placeSchoolName.get("place");
        String school = placeSchoolName.get("school");
        String[] fileNameList = schoolProfessionService.getFileNameList(place, school);
        for (String item : fileNameList) {
            System.out.println(item);
        }
        return fileNameList;
    }

    @PreAuthorize("@ss.hasPermi('exam:download')")
    @GetMapping("/downloadFile/{fileName}")
    public ResponseEntity<Resource> download(@PathVariable String fileName) throws IOException {
        Path path = Paths.get("D:\\path\\to\\save\\北京\\北京大学\\" + fileName);
        Resource resource = new UrlResource(path.toUri());
        if (resource.exists() || resource.isReadable()) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}