package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MyProvince;

/**
 * 省市Service接口
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
public interface IMyProvinceService 
{
    /**
     * 查询省市
     * 
     * @param id 省市主键
     * @return 省市
     */
    public MyProvince selectMyProvinceById(Long id);

    /**
     * 查询省市列表
     * 
     * @param myProvince 省市
     * @return 省市集合
     */
    public List<MyProvince> selectMyProvinceList(MyProvince myProvince);

    /**
     * 新增省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    public int insertMyProvince(MyProvince myProvince);

    /**
     * 修改省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    public int updateMyProvince(MyProvince myProvince);

    /**
     * 批量删除省市
     * 
     * @param ids 需要删除的省市主键集合
     * @return 结果
     */
    public int deleteMyProvinceByIds(Long[] ids);

    /**
     * 删除省市信息
     * 
     * @param id 省市主键
     * @return 结果
     */
    public int deleteMyProvinceById(Long id);
}
