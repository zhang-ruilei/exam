package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MyProvinceMapper;
import com.ruoyi.system.domain.MyProvince;
import com.ruoyi.system.service.IMyProvinceService;

/**
 * 省市Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
@Service
public class MyProvinceServiceImpl implements IMyProvinceService 
{
    @Autowired(required = false)
    private MyProvinceMapper myProvinceMapper;

    /**
     * 查询省市
     * 
     * @param id 省市主键
     * @return 省市
     */
    @Override
    public MyProvince selectMyProvinceById(Long id)
    {
        return myProvinceMapper.selectMyProvinceById(id);
    }

    /**
     * 查询省市列表
     * 
     * @param myProvince 省市
     * @return 省市
     */
    @Override
    public List<MyProvince> selectMyProvinceList(MyProvince myProvince)
    {
        return myProvinceMapper.selectMyProvinceList(myProvince);
    }

    /**
     * 新增省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    @Override
    public int insertMyProvince(MyProvince myProvince)
    {
        return myProvinceMapper.insertMyProvince(myProvince);
    }

    /**
     * 修改省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    @Override
    public int updateMyProvince(MyProvince myProvince)
    {
        return myProvinceMapper.updateMyProvince(myProvince);
    }

    /**
     * 批量删除省市
     * 
     * @param ids 需要删除的省市主键
     * @return 结果
     */
    @Override
    public int deleteMyProvinceByIds(Long[] ids)
    {
        return myProvinceMapper.deleteMyProvinceByIds(ids);
    }

    /**
     * 删除省市信息
     * 
     * @param id 省市主键
     * @return 结果
     */
    @Override
    public int deleteMyProvinceById(Long id)
    {
        return myProvinceMapper.deleteMyProvinceById(id);
    }
}
