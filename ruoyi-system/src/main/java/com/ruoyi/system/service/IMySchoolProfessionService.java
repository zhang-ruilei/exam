package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.MySchoolProfession;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-06-09
 */
public interface IMySchoolProfessionService {

    public  List<MySchoolProfession> selectMySchoolProfessionByPlace(String province);

    public List<MySchoolProfession> selectMySchoolProfessionList(MySchoolProfession mySchoolProfession);

    public int insertMySchoolProfession(MySchoolProfession mySchoolProfession);

    public int updateMySchoolProfession(MySchoolProfession mySchoolProfession);

    public int deleteMySchoolProfessionByIDs(Long[] IDs);

    public int deleteMySchoolProfessionByID(Long ID);
}
