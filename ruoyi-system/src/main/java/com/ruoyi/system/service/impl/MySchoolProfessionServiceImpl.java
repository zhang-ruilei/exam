package com.ruoyi.system.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MySchoolProfessionMapper;
import com.ruoyi.system.domain.MySchoolProfession;
import com.ruoyi.system.service.IMySchoolProfessionService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-06-09
 */
@Service
public class MySchoolProfessionServiceImpl implements IMySchoolProfessionService {
    @Autowired(required = false)
    private MySchoolProfessionMapper mySchoolProfessionMapper;

    @Override
    public  List<MySchoolProfession> selectMySchoolProfessionByPlace(String province) {
        return mySchoolProfessionMapper.selectMySchoolProfessionByPlace(province);
    }

    @Override
    public List<MySchoolProfession> selectMySchoolProfessionList(MySchoolProfession mySchoolProfession) {
        return mySchoolProfessionMapper.selectMySchoolProfessionList(mySchoolProfession);
    }

    @Override
    public int insertMySchoolProfession(MySchoolProfession mySchoolProfession) {
        return mySchoolProfessionMapper.insertMySchoolProfession(mySchoolProfession);
    }


    @Override
    public int updateMySchoolProfession(MySchoolProfession mySchoolProfession) {
        return mySchoolProfessionMapper.updateMySchoolProfession(mySchoolProfession);
    }

    @Override
    public int deleteMySchoolProfessionByIDs(Long[] IDs) {
        return mySchoolProfessionMapper.deleteMySchoolProfessionByIDs(IDs);
    }

    @Override
    public int deleteMySchoolProfessionByID(Long ID) {
        return mySchoolProfessionMapper.deleteMySchoolProfessionByID(ID);
    }

    public  String[] getFileNameList(String place,String school) {
        // 指定文件夹路径
        String folderPath = "D:\\path\\to\\save\\"+place+"\\"+school+"";
        // 创建文件夹对象
        File folder = new File(folderPath);
        // 获取文件夹中所有文件名
        return folder.list();
    }
}
