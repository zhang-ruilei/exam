package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.MySchoolProfession;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-06-09
 */
public interface MySchoolProfessionMapper {

    public List<MySchoolProfession> selectMySchoolProfessionList(MySchoolProfession mySchoolProfession);

    public int insertMySchoolProfession(MySchoolProfession mySchoolProfession);

    public int updateMySchoolProfession(MySchoolProfession mySchoolProfession);

    public int deleteMySchoolProfessionByID(Long ID);

    public int deleteMySchoolProfessionByIDs(Long[] IDs);

    List<MySchoolProfession> selectMySchoolProfessionByPlace(String place);
}
