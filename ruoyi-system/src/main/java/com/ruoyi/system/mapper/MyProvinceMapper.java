package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MyProvince;

/**
 * 省市Mapper接口
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
public interface MyProvinceMapper 
{
    /**
     * 查询省市
     * 
     * @param id 省市主键
     * @return 省市
     */
    public MyProvince selectMyProvinceById(Long id);

    /**
     * 查询省市列表
     * 
     * @param myProvince 省市
     * @return 省市集合
     */
    public List<MyProvince> selectMyProvinceList(MyProvince myProvince);

    /**
     * 新增省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    public int insertMyProvince(MyProvince myProvince);

    /**
     * 修改省市
     * 
     * @param myProvince 省市
     * @return 结果
     */
    public int updateMyProvince(MyProvince myProvince);

    /**
     * 删除省市
     * 
     * @param id 省市主键
     * @return 结果
     */
    public int deleteMyProvinceById(Long id);

    /**
     * 批量删除省市
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMyProvinceByIds(Long[] ids);
}
