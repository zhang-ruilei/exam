import request from '@/utils/request'

// 查询省市列表
export function listProvince(query) {
  return request({
    url: '/system/province/list',
    method: 'get',
    params: query
  })
}

// 查询省市详细
export function getProvince(id) {
  return request({
    url: '/system/province/' + id,
    method: 'get'
  })
}

// 新增省市
export function addProvince(data) {
  return request({
    url: '/system/province',
    method: 'post',
    data: data
  })
}

// 修改省市
export function updateProvince(data) {
  return request({
    url: '/system/province',
    method: 'put',
    data: data
  })
}

// 删除省市
export function delProvince(id) {
  return request({
    url: '/system/province/' + id,
    method: 'delete'
  })
}

// 查询【请填写功能名称】列表
export function listProfession(query) {
  return request({
    url: '/system/profession/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getProfession(ID) {
  return request({
    url: '/system/profession/' + ID,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addProfession(data) {
  return request({
    url: '/system/profession',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateProfession(data) {
  return request({
    url: '/system/profession',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delProfession(ID) {
  return request({
    url: '/system/profession/' + ID,
    method: 'delete'
  })
}

export function listSchool(query) {
  return request({
    url: '/system/school/list',
    method: 'get',
    params: query
  })
}

export function uploadFile(file) {
  return request({
    url: '/exam/uploadFile',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: file
  })
}

export function findSchool(place) {
  return request({
    url: '/exam/findSchool',
    method: 'post',
    data: place
  })
}

export function getFileNameList(place, school) {
  return request({
    url: '/exam/getfilelist',
    method: 'post',
    data: {place, school}
  })
}

export async function downloadFile(fileName) {
  return request({
    method: 'get',
    url: '/exam/downloadFile/'+ fileName,
    responseType: 'stream',
  })
}
