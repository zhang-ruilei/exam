<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-d3d0a9303e11d522a06cd263f3079027715.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi v3.8.5</h1>
<h2 align="center">基于若依的考试真题共产系统</h2>


## 平台简介

本系统是由于考研真题难以寻觅而创建。

* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，辅助chat-gpt完成。
* 特别鸣谢：若依若作者团队以及努力拼搏和善良的各位。
* 祝愿：梦想成真，善良健康开心。

## 内置功能

1.  若依权限管理功能
### 2. 共产真题：用户可以上传真题，下载真题，作者个人保证用不收费，供各种考试党使用。

使用说明：两个用户：共产主义接班人使用ry账号，可进行文档的下载和分享。
                admin用户进行管理，尚未完成可视化文档审阅功能，即admin用户审核ry用户上传文档的功能。

欢迎广大网友共同开发，qq不定期不会上，加不加全看缘分。 qq：374758035
## 演示图
图片在MDpic文件夹


https://gitee.com/zhang-ruilei/exam/blob/master/MDpic/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(1).png

https://gitee.com/zhang-ruilei/exam/blob/master/MDpic/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(3).png

https://gitee.com/zhang-ruilei/exam/blob/master/MDpic/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(4).png

https://gitee.com/zhang-ruilei/exam/blob/master/MDpic/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(5).png

